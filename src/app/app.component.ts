import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  contador: number = 10;
  title = 'TP-Mod2-Clemente';

  valorActualizado(contadorActualizado: number) {
    this.contador = contadorActualizado;
  }
}
