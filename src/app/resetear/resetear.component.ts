import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-resetear',
  templateUrl: './resetear.component.html',
  styleUrls: ['./resetear.component.css'],
})
export class ResetearComponent implements OnInit {
  constructor() {}

  @Input() contador!: number;
  @Output() contadorActualizado = new EventEmitter();

  ngOnInit(): void {}

  reset() {
    this.contadorActualizado.emit(10);
  }
}
