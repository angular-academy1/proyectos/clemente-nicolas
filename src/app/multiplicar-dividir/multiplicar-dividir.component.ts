import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiplicar-dividir',
  templateUrl: './multiplicar-dividir.component.html',
  styleUrls: ['./multiplicar-dividir.component.css'],
})
export class MultiplicarDividirComponent implements OnInit {
  constructor() {}

  @Input() contador!: number;
  @Output() contadorActualizado = new EventEmitter();

  ngOnInit(): void {}

  multiplicar() {
    this.contadorActualizado.emit(this.contador * 2);
  }

  dividir() {
    this.contadorActualizado.emit(this.contador / 2);
  }

  valorActualizado(contadorActualizado: number) {
    this.contador = contadorActualizado;
    this.contadorActualizado.emit(this.contador);
  }
}
