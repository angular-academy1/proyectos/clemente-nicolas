import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-incrementar-decrementar',
  templateUrl: './incrementar-decrementar.component.html',
  styleUrls: ['./incrementar-decrementar.component.css'],
})
export class IncrementarDecrementarComponent implements OnInit {
  constructor() {}

  @Input() contador!: number;
  @Output() contadorActualizado = new EventEmitter();

  ngOnInit() {}

  incrementar() {
    this.contadorActualizado.emit(this.contador + 1);
  }

  decrementar() {
    this.contadorActualizado.emit(this.contador - 1);
  }

  valorActualizado(contadorActualizado: number) {
    this.contador = contadorActualizado;
  }
}
